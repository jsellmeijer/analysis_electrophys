%   Copyright 2017 Jim Sellmeijer
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.



tic
format long

for m = 1:1:length(rec_date) 
    
    %cd to the recording date folder
    cd([file_loc{m} '\' condition{m} '\' ])    
    datevec(rec_date(m));
    temp_date = datevec(rec_date(m)); % temp date is used to create the string below
    cd([file_loc{m} '\' condition{m} '\recordings ', num2str(temp_date(3)),'-',num2str(temp_date(2)),'-',num2str(temp_date(1)-2000)])    
    
    %load the file
    load([cell_name{m}, ' ', num2str(temp_date(3)),'-',num2str(temp_date(2)),'-',num2str(temp_date(1)-2000),'.mat']) % load the file by creating a string with the name and date values
    
    eval(['trace = cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch1.values'';'])
    eval(['times = cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch3.times;'])
    eval(['spikes = cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch3.values;'])
    eval(['samp_freq(m) = 1/cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch1.interval'';'])% sampling rate is defined by the data
    
    %clear the loaded variables to free memory
    eval(['clear cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch1;'])
    eval(['clear cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch2;'])
    eval(['clear cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch3;'])
    eval(['clear cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch4;'])
    eval(['clear cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch5;'])
    eval(['clear cell_',cell_name{m}(end), '_', num2str(temp_date(3)),'_',num2str(temp_date(2)),'_',num2str(temp_date(1)-2000),'_Ch31;'])

    rec_length = 90;
    
%Single-unit processing    
    
    %calculate firing rate
    temp_window = times(times>wind_start(m) & times<wind_start(m)+rec_length);
    fire_freq(m) = length(temp_window)/(temp_window(end)-temp_window(1));
    
    %calculate bursting activity
    ISI_for_burst = 50*10^-3;
    Spike.T = temp_window;
    [Burst{m}, SpikeBurstNumber{m}] = BurstDetectISIn(Spike, 3, ISI_for_burst);
    if ~isempty(Burst{m}.S)
        burst_length(m) = mean(Burst{m}.S);
        number_of_bursts(m) = length(Burst{m}.S);
    else
        burst_length(m) = 0;
        number_of_bursts(m) = 0;
    end

    temp_window_rep_burst = temp_window;

    %replace bursts by single APs
    for i = 1:length(Burst{m}.T_start)

        %calculate the middle of the burst and make the first entry of the
        %burst into the average time of the burst APs
        temp_window_rep_burst(find(temp_window_rep_burst>=Burst{m}.T_start(i) & temp_window_rep_burst<=Burst{m}.T_end(i),1)) = mean(temp_window_rep_burst(find(temp_window_rep_burst>=Burst{m}.T_start(i) & temp_window_rep_burst<=Burst{m}.T_end(i))));
        %delete the rest
        temp_window_rep_burst(find(temp_window_rep_burst>=Burst{m}.T_start(i) & temp_window_rep_burst<=Burst{m}.T_end(i),length(find(temp_window_rep_burst>=Burst{m}.T_start(i) & temp_window_rep_burst<=Burst{m}.T_end(i)))-1,'last'))=[];

    end

    %calculate the firing rate for single APs
    fire_freq_rep_burst(m) = length(temp_window_rep_burst)/(temp_window_rep_burst(end)-temp_window_rep_burst(1));

    %delete variables that are remade
    %for single unit
    clear trace
    clear times
    clear spikes
    clear temp_window
    clear temp_window_rep_burst
    

end

cd(file_loc{1});
filename = 'output_file'
save(filename)
toc